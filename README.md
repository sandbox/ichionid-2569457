Guardian-news
=============
This is a drupal-7 module which retrieves data from the Guardian newspaper api.
Check the api at http://explorer.content.guardianapis.com/?api-key=test

The information fetched by guardian are rendered in a block using angular.js.

Get Started
=============
The open platform is a public service for accessing all the content the
theguardian creates, categorised by tag and section.
There are over one and a half million items availablepublished as far back as 
1999.

Read through the documentation online at
http://open-platform.theguardian.com/access/

There you will be allowed to:
 --Familiarise yourself with the API using our explorer
 --Get access by registering for a key and reviewing our terms and conditions
